<?php

    require __DIR__ . '/vendor/autoload.php';

    \Ratchet\Client\connect('ws://127.0.0.1:8888')->then(function($conn) {
        $conn->on('message', function($msg) use ($conn) {
            echo "Received: {$msg}\n";
        });
        $conn->send('{"type":"publish", "channel":"teste", "message":"This is a message"}');
        $conn->close();
    }, function ($e) {
        echo "Could not connect: {$e->getMessage()}\n";
    });