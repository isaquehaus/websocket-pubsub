<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use MyApp\Channel;

class Chat implements MessageComponentInterface {
    protected $clients;
    protected $channels;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->channels = [];
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $msg_obj = json_decode($msg);

        if (!$msg_obj) return;

        $type = $msg_obj->type;
        switch ($type) {
            case 'subscribe':
                $this->subscribe($msg_obj->channel, $from);
                break;
            case 'unsubscribe':
                $this->subscribe($msg_obj->channel, $from, true);
                break;
            case 'publish':
                $this->publish($msg_obj->channel, $from, $msg_obj->message);
                break;
            default:
                break;
        }
    }

    private function subscribe($channel, ConnectionInterface $from, $remove = false) {
        if(!$remove) {
            if (!isset($this->channels[$channel][$from->resourceId])) {
                $this->channels[$channel][$from->resourceId] = $from;
                echo "{$from->resourceId} was subscribed on channel {$channel}\n";
            }
        } else {
            if (isset($this->channels[$channel][$from->resourceId])) {
                unset($this->channels[$channel][$from->resourceId]);
                echo "{$from->resourceId} was unsubscribed from channel {$channel}\n";
            }
        }
    }

    private function print() {
        foreach ($this->channels as $name => $channel) {
            echo "{$name}[";
            foreach ($channel as $key => $client) {
                echo "{$key} ";
            }
            echo "]\n";
        }
    }

    private function removeClientFromAllChannels(ConnectionInterface $conn) {
        foreach ($this->channels as $channel => $array) {
            $this->subscribe($channel, $conn, true);
        }
    }

    private function publish($channel, ConnectionInterface $from, $message) {
        if (isset($this->channels[$channel]) and is_array($this->channels[$channel])) {
            foreach ($this->channels[$channel] as $client) {
                $client->send($message);
            }
            echo "{$from->resourceId} has published on channel {$channel} the message: {$message}\n";
        }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        $this->removeClientFromAllChannels($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }
}
